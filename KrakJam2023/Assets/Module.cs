using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : MonoBehaviour
{
    [SerializeField]
    Material green;
    [SerializeField]
    Material black;
    Renderer renderer;

    private void Start()
    {
        renderer = GetComponent<Renderer>();
    }


    public void SetMaterial(Material mat)
    {
        renderer.material = mat;
    }

    public void ResetColor()
    {

        renderer.material = null;
    }

}
