using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public class ModuleManager : MonoBehaviour{


	[SerializeField] List<Module> modules;
    [SerializeField] private IEnumerator coroutine;
    [SerializeField] List<Material> materials;
	[SerializeField] float changeSpeed;
	public void Start()
	{
		coroutine = ChangeRandomColor();
		StartCoroutine(coroutine);
	}
  


    IEnumerator ChangeRandomColor()
    {
		int randomModuleIndex;
		int randomMatIndex;



        for (; ; )
		{




			 randomModuleIndex = Random.Range(0, modules.Count);
			 randomMatIndex = Random.Range(0, materials.Count);


         

            modules[randomModuleIndex].SetMaterial(materials[randomMatIndex]);

			yield return new WaitForSeconds(changeSpeed);
		}
		
        }
    
}
