using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class VFXController : MonoBehaviour{

	[SerializeField] private GameObject vfxPrefab;
	public void HexLocked(Transform rootForVFX){
		Instantiate(vfxPrefab, rootForVFX);
	}
}
