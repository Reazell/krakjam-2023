using System;

using Cinemachine;

using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class CameraController : MonoBehaviour{
	[SerializeField]
	float maxZ = 10.0f;
	[SerializeField]
	float minZ = -10.0f;
	[SerializeField]
	float maxX = 10.0f;
	[SerializeField]
	float minX = -10.0f;

	[SerializeField] private GameObject cameraAnchor;
	private CinemachineVirtualCamera _cinemachina;

	public float scrollingSpeed = 0.2f;

	private void Start(){
		_cinemachina = GetComponent<CinemachineVirtualCamera>();
	}

	void FixedUpdate(){
		if(Input.GetKey(KeyCode.W) & transform.position.z < maxZ){
			cameraAnchor.transform.position += new Vector3(0, 0, scrollingSpeed);
			FollowTransform(cameraAnchor.transform);
		}

		if(Input.GetKey(KeyCode.S) & transform.position.z > minZ){
			cameraAnchor.transform.position += new Vector3(0, 0, -scrollingSpeed);
			FollowTransform(cameraAnchor.transform);
		}

		if(Input.GetKey(KeyCode.D) & transform.position.x < maxX){
			cameraAnchor.transform.position += new Vector3(scrollingSpeed, 0, 0);
			FollowTransform(cameraAnchor.transform);
		}

		if(Input.GetKey(KeyCode.A) & transform.position.x > minX){
			cameraAnchor.transform.position += new Vector3(-scrollingSpeed, 0, 0);
			FollowTransform(cameraAnchor.transform);
		}
	}

	public void FollowTransform(Transform trsf){
		if(_cinemachina.Follow.Equals(trsf)){
			return;
		}

		if(trsf.gameObject.Equals(cameraAnchor)){
			trsf.position += new Vector3(0, 6, 0);
		}

		_cinemachina.Follow = trsf;
		cameraAnchor.transform.position = trsf.position;
	}
}
