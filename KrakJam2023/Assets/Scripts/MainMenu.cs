using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour{

	public void StartGame(){
		SceneManager.LoadScene("Level2", LoadSceneMode.Single);
	}
	
	public void Exit(){
		Application.Quit();
	}
}
