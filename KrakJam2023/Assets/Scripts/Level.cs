﻿using UnityEngine;

public class Level : MonoBehaviour{

	[SerializeField] private CoreLoop coreLoop;
	
	public void EndTurn(){
		coreLoop.EndTurn();
	}

	
}