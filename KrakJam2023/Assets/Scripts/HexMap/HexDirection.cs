﻿namespace HexMap {
	public enum HexDirection {
		TopLeft,
		TopRight,
		MidRight,
		BotRight,
		BotLeft,
		MidLeft
	}
}