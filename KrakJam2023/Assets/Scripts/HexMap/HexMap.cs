using System;
using System.Collections.Generic;
using System.Linq;

using Sirenix.OdinInspector;

using UnityEngine;
using UnityEngine.Serialization;

namespace HexMap{
public class HexMap : SerializedMonoBehaviour{

	[SerializeField] private GameObject hexPrefab;
	[SerializeField] private float hexOuterRadius = 1.1f;
	[SerializeField] private Vector3 originPoint;
	[SerializeField] private int xSize;
	[SerializeField] private int ySize;
	[SerializeField] private int startingX;
	[SerializeField] private int startingY;

	[SerializeField] [HideInInspector] private Node[,] _hexCells;
	[SerializeField] [HideInInspector] private List<Node> _nodeMap = new();

	private Node _lastBotLeftNeighbour;
	private Node _lastBotRightNeighbour;

	private bool isStartingPointInitialized = false;

	private void LateUpdate(){
		if(!isStartingPointInitialized){
			CaptureStartingHex();
			isStartingPointInitialized = true;
		}
	}

	[Button("Generate map")]
	public void InitializeMap(){
		RemoveOldCells();
		_hexCells = new Node[xSize, ySize];
		PopulateMap();
	}

	[Button("Clear map")]
	public void RemoveOldCells(){
		foreach(var node in _nodeMap){
			DestroyImmediate(node.gameObject);
		}

		_nodeMap.Clear();
	}

	private void PopulateMap(){
		for(var x = 0; x < xSize; x++){
			for(var y = 0; y < ySize; y++){
				Node node;
				if(y % 2 == 0){
					node = InstantiateHexPrefab(x, y);
				} else{
					node = InstantiateHexPrefab(x - 1, y);
				}

				if(y == ySize - 1){
					node.IsWinningNode = true;
				}

				_hexCells[x, y] = node;
			}
		}
	}

	private Node InstantiateHexPrefab(int x, int y){
		var hexInnerRadius = hexOuterRadius * 0.866025404f;
		var spawnPosition = originPoint;

		// In unity worldspace the grid extends through x and z, y is camera perspective
		spawnPosition.x = (x + y * 0.5f - y / 2) * (hexInnerRadius * 2f);
		spawnPosition.z = y * hexOuterRadius * 1.5f * -1;

		var instantiatedNode = Instantiate(hexPrefab, spawnPosition, Quaternion.identity);
		instantiatedNode.transform.parent = gameObject.transform;
		var nodeComponent = instantiatedNode.GetComponent<Node>();
		_nodeMap.Add(nodeComponent);
		nodeComponent.HexMap = this;
		nodeComponent.XIndex = x;
		nodeComponent.YIndex = y;
		return nodeComponent;
	}

	private void CaptureStartingHex(){
		var startingCell = _hexCells[startingX, startingY];
		if(startingCell == null){
			return;
		}

		startingCell.SetVisible();
		startingCell.LockToPlayer();
	}

	public List<Node> GetAllPossibleCellNeighbours(Node source){
		var list = new List<Node>{
			GetCellNeighbour(source, HexDirection.BotLeft),
			GetCellNeighbour(source, HexDirection.BotRight),
			GetCellNeighbour(source, HexDirection.MidLeft),
			GetCellNeighbour(source, HexDirection.MidRight),
			GetCellNeighbour(source, HexDirection.TopLeft),
			GetCellNeighbour(source, HexDirection.TopRight)
		};

		return list.Where(node => node != null)
				   .ToList();
	}

	public Node GetCellNeighbour(Node source, HexDirection direction){
		var neighbourIndexOffset = GetHexNeighbourIndexOffset(direction);

		var neighbourXIndex = source.XIndex + neighbourIndexOffset.Item1;
		var neighbourYIndex = source.YIndex + neighbourIndexOffset.Item2;

		if(neighbourYIndex % 2 == 1 && 
		   direction.Equals(HexDirection.MidRight)){
			neighbourXIndex += 1;
		} else if(neighbourYIndex % 2 == 0 && 
				  direction.Equals(HexDirection.MidLeft)){
			neighbourXIndex -= 1;
		}

		try{
			return _hexCells[neighbourXIndex, neighbourYIndex];
		} catch(IndexOutOfRangeException){
			return null;
		}
	}

	private(int, int) GetHexNeighbourIndexOffset(HexDirection direction){
		var neighbourOffset = direction switch{
								  HexDirection.TopLeft => (0, -1),
								  HexDirection.TopRight => (1, -1),
								  HexDirection.MidLeft => (0, 0),
								  HexDirection.MidRight => (1, 0),
								  HexDirection.BotLeft => (0, 1),
								  HexDirection.BotRight => (1, 1),
								  _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
							  };

		return neighbourOffset;
	}

	public Node LastBotLeftNeighbour{
		get => _lastBotLeftNeighbour;
		set => _lastBotLeftNeighbour = value;
	}
	public Node LastBotRightNeighbour{
		get => _lastBotRightNeighbour;
		set => _lastBotRightNeighbour = value;
	}
}
}
