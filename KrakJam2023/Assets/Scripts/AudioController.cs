using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour{

	[SerializeField] private List<AudioClip> hexLockedSounds;

	private AudioSource _audioSource;

	private void Start(){
		_audioSource = GetComponent<AudioSource>();
	}

	public void PlayHexLockedSound(){
		var randomNumber = Random.Range(0, hexLockedSounds.Count - 1);
		_audioSource.clip = hexLockedSounds[randomNumber];
		_audioSource.Play();
	}
}
