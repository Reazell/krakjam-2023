using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EmissionPulser : MonoBehaviour
{
    [SerializeField]
    public Color emissionColor;
    [SerializeField]
    Material emissionMat;
    [SerializeField]
    public float emissionIntesity = 1.0f;
    [SerializeField]
    public float speed = 1.0f;
    float time;
    [SerializeField]
    float offset = 0.0f;
    
 private void Awake()
    {
    
    }
    void Update()
    {
        time += Time.deltaTime;
        float waveIntensity;
        waveIntensity = Mathf.Abs(emissionIntesity * Mathf.Sin(speed * time) + offset);
        emissionMat.SetColor("_EmissionColor", emissionColor * waveIntensity); 
       
    }
}
