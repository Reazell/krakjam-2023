using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class HexSpawner : MonoBehaviour {
	[SerializeField] public GameObject hexPrefab;

	void Start() { }


	[Button("spawn")]
	void Spawn() {
		float offset = 1.0f;
		bool phase = false;
		for (int i = 0; i < 10; i++) {
			if (phase) {
				offset = 0;
			} else {
				offset = 1.0f;
			}

			for (int j = 0; j < 10; j++) {
				{
					Instantiate(hexPrefab, new Vector3(j * 2 + offset, 0, 1.69f * i), Quaternion.identity);
				}
			}

			phase = !phase;
			Debug.Log(offset);
		}
	}
}