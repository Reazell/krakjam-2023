﻿using System;

using UnityEngine;

namespace NodeTypes{

[Serializable]
public class NodeTypeHolder{

	private NodeTypeEnum _nodeTypeEnum;
	private NodeType _nodeTypeObject;
	private PlayerStats _playerStats;

	public void ResolveNodeType(){
		_nodeTypeObject = _nodeTypeEnum switch{
							  NodeTypeEnum.Enemy => new EnemyNode(),
							  NodeTypeEnum.CurrencyMine => new CurrencyMineNode(),
							  NodeTypeEnum.Standard => new StandardNode(),
							  _ => throw new ArgumentOutOfRangeException()
						  };
	}

	public void WasClicked(){
		if(!_nodeTypeObject.IsLockable){
			return;
		}

		_playerStats.BillPlayerForLockingHex(_nodeTypeObject.LockCost);
		switch(_nodeTypeEnum){
			case NodeTypeEnum.Standard:
				return;
			case NodeTypeEnum.CurrencyMine:
				RegisterAcquiredMine();
				break;
		}
	}

	private void RegisterAcquiredMine(){
		_playerStats.AddCurrencyMine(_nodeTypeObject as CurrencyMineNode);
	}

	public NodeTypeEnum NodeTypeEnum{
		get => _nodeTypeEnum;
		set => _nodeTypeEnum = value;
	}

	public PlayerStats PlayerStats{
		get => _playerStats;
		set => _playerStats = value;
	}
}
}
