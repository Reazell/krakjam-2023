﻿using System;

namespace NodeTypes{
[Serializable]
public abstract class NodeType{

	public bool IsLockable{ get; set; }
	public int LockCost{ get; set; }
}
}
