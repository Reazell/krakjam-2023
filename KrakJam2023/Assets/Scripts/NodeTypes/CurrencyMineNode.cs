﻿using System;

using UnityEngine;

namespace NodeTypes{
[Serializable]
public class CurrencyMineNode : NodeType{

	private int _minedAmount = 10;
	private Color _nodeColor = Color.yellow;

	public CurrencyMineNode(){
		IsLockable = true;
		LockCost = 10;
	}

	public int GetEarnedCurrency(){
		return _minedAmount;
	}

}
}
