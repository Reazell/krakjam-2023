using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour{

	public void GoToLevel3(){
		SceneManager.LoadScene("Level3", LoadSceneMode.Single);
	}
	public void GoToLevel4(){
		SceneManager.LoadScene("Level4", LoadSceneMode.Single);
	}
	public void GoToCredits(){
		SceneManager.LoadScene("Credits", LoadSceneMode.Single);
	}
	public void GoToMainMenu(){
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}
}
