﻿using System;
using System.Collections.Generic;

using NodeTypes;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerStats : MonoBehaviour{

	private List<CurrencyMineNode> _playerCurrencyMines;
	private Node _playerPosition;
	[SerializeField] private int currencyValue;

	[SerializeField] private TextMeshProUGUI pointsDisplayText;
	 
	private void Awake(){
		_playerCurrencyMines = new List<CurrencyMineNode>();
	}

	public void AddCurrencyMine(CurrencyMineNode mine){
		_playerCurrencyMines.Add(mine);
	}

	public void RemoveCurrencyMine(CurrencyMineNode mine){
		_playerCurrencyMines.Remove(mine);
	}

	public void Update() {
		pointsDisplayText.text = currencyValue.ToString();
	}

	public void UpdatePlayerCurrencyAmount(){
		if(_playerCurrencyMines.Count == 0){
			return;
		}

		foreach(var currencyMine in _playerCurrencyMines){
			currencyValue += currencyMine.GetEarnedCurrency();
		}
	}

	public void BillPlayerForLockingHex(int amount){
		currencyValue -= amount;
	}

	public int CurrencyValue => currencyValue;
}