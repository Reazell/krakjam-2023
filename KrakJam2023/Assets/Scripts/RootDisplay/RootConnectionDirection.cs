﻿namespace RootDisplay {
	public enum RootConnectionDirection {
		None,
		Left,
		Right
	}
}