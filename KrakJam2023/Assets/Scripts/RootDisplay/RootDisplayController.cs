﻿using RootDisplay;
using Sirenix.OdinInspector;
using UnityEngine;

public class RootDisplayController : MonoBehaviour {
	[SerializeField] private GameObject _leftToLeft;
	[SerializeField] private GameObject _leftToRight;
	[SerializeField] private GameObject _rightToLeft;
	[SerializeField] private GameObject _rightToRight;
	[SerializeField] private GameObject _leftToMid;
	[SerializeField] private GameObject _rightToMid;
	
	[SerializeField] private GameObject _mainTreeMid;
	[SerializeField] private GameObject _mainTreeLeft;
	[SerializeField] private GameObject _mainTreeRight;

	[SerializeField] private Transform rootMeshesParent;
	[SerializeField] private Transform keepEnabled;

	private bool _isMainRoot;
	
	private RootConnectionDirection _upperConnection;
	private RootConnectionDirection _lowerConnection;

	public void EnableMainRoot() {
		_mainTreeMid.SetActive(true);
		_isMainRoot = true;
	}
	
	public void SetUpperConnection(RootConnectionDirection direction) {
		_upperConnection = direction;
		UpdateRootDisplay();
	}

	public void SetLowerConnection(RootConnectionDirection direction) {
		_lowerConnection = direction;
		UpdateRootDisplay();
	}

	private void UpdateRootDisplay() {

		if (_isMainRoot && _lowerConnection == RootConnectionDirection.None) {
			return;
		}
		
		DisableAllRootMeshes();
		
		if (_isMainRoot && _lowerConnection == RootConnectionDirection.Left){
			_mainTreeLeft.SetActive(true);
		}
		
		else if (_isMainRoot && _lowerConnection == RootConnectionDirection.Right){
			_mainTreeRight.SetActive(true);
		}
		
		else if (_upperConnection == RootConnectionDirection.Left &&
			_lowerConnection == RootConnectionDirection.Left) {
			_leftToLeft.SetActive(true);
		}

		else if (_upperConnection == RootConnectionDirection.Left &&
		    _lowerConnection == RootConnectionDirection.Right) {
			_leftToRight.SetActive(true);
		}
		
		else if (_upperConnection == RootConnectionDirection.Right &&
		        _lowerConnection == RootConnectionDirection.Left) {
			_rightToLeft.SetActive(true);
		}
		
		else if (_upperConnection == RootConnectionDirection.Right &&
		        _lowerConnection == RootConnectionDirection.Right) {
			_rightToRight.SetActive(true);
		}
		
		else if (_upperConnection == RootConnectionDirection.Left &&
		        _lowerConnection == RootConnectionDirection.None) {
			_leftToMid.SetActive(true);
		}
		
		else if (_upperConnection == RootConnectionDirection.Right &&
		        _lowerConnection == RootConnectionDirection.None) {
			_rightToMid.SetActive(true);
		}
	}

	private void DisableAllRootMeshes() {
		foreach (Transform rootMesh in rootMeshesParent) {
			rootMesh.gameObject.SetActive(false);
		}
		keepEnabled.gameObject.SetActive(true);
	}
}