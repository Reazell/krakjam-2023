﻿using System;
using System.Collections.Generic;
using System.Linq;
using NodeTypes;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace HexUI {
	public class HexUIController : SerializedMonoBehaviour {

		[SerializeField] [HideInInspector] private NodeTypeEnum nodeType;
		
		[SerializeField] private TextMeshProUGUI costText;
		
		[SerializeField] private Dictionary<NodeTypeEnum, GameObject> _nodeIconToPrefabInstanceMap;
		
		private GameObject _currentlyActiveInstance;
		
		public void UpdateUIDisplay() {
			var cost = GetNodeTypeCost(nodeType);
			costText.text = cost.ToString();

			if (_currentlyActiveInstance) {
				_currentlyActiveInstance.SetActive(false);
			}

			if (!_nodeIconToPrefabInstanceMap.Keys.Contains(this.NodeType)) {
				return;
			}

			var prefabInstanceToEnable = _nodeIconToPrefabInstanceMap[nodeType];
			_currentlyActiveInstance = prefabInstanceToEnable;
			prefabInstanceToEnable.SetActive(true);
		}

		private int GetNodeTypeCost(NodeTypeEnum nodeType) {
			var nodeCost = nodeType switch{
				NodeTypeEnum.Enemy => new EnemyNode().LockCost,
				NodeTypeEnum.CurrencyMine => new CurrencyMineNode().LockCost,
				NodeTypeEnum.Standard => new StandardNode().LockCost,
				_ => throw new ArgumentOutOfRangeException(nameof(nodeType), nodeType,
					"Unrecognized node type, can't load node type cost")
			};
			return nodeCost;
		}
		
		public NodeTypeEnum NodeType {
			get => nodeType;
			set => nodeType = value;
		}
	}
}