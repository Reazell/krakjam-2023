using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextGenerator : MonoBehaviour
{
    float randomNumber;
    [SerializeField] TMP_Text text;
    void Start() {
        randomNumber = Random.Range(1, 100);
        if (randomNumber <= 50)
            text.text = "DUPA";
        if (randomNumber > 50)
            text.text = "CYCKI";
    }

}
