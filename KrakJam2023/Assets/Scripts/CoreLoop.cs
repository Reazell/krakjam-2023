﻿using System;

using UnityEngine;
using UnityEngine.Events;

public class CoreLoop : MonoBehaviour{

	[SerializeField] private SceneLoader sceneLoader;

	private PlayerStats _playerStats;
	
	private UnityEvent _endTurnEvent;
	private UnityEvent _startTurnEvent;
	private UnityEvent _endGameEvent;
	private UnityEvent _startGameEvent;

	[SerializeField] private string nextScene = "Level3";
	
	private void Start() {
		GetReferences();
		_startGameEvent?.Invoke();
	}
	
	private void GetReferences() {
		_playerStats = FindObjectOfType<PlayerStats>();
	}

	public void EndTurn() {
		UpdatePlayerCurrency();
		//calculate all data
		//calculate enemy move
		//update all objects state
		_endTurnEvent?.Invoke();
		_startTurnEvent?.Invoke();
	}

	private void UpdatePlayerCurrency() {
		_playerStats.UpdatePlayerCurrencyAmount();
		Debug.Log("MONEEEEEY: " + _playerStats.CurrencyValue);
		if(_playerStats.CurrencyValue <= 0){
			// GameEnd();
		}
	}

	public void GameEnd() {
		Debug.Log("game end!");
		if(nextScene.Equals("Level3")){
			nextScene = "Level4";
			sceneLoader.GoToLevel3();
		}else if(nextScene.Equals("Level4")){
			nextScene = "Credits";
			sceneLoader.GoToLevel4();
		}else if(nextScene.Equals("Credits")){
			nextScene = "MainMenu";
			sceneLoader.GoToCredits();
		}else if(nextScene.Equals("MainMenu")){
			nextScene = "Level3";
			sceneLoader.GoToMainMenu();
		}
	}

}