﻿using System;
using HexMap;
using HexUI;
using NodeTypes;
using RootDisplay;
using Sirenix.OdinInspector;

using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Collider))]
public class Node : SerializedMonoBehaviour {

	[SerializeField] private NodeTypeEnum nodeTypeEnum;
	[SerializeField] private NodeTypeHolder nodeTypeHolder;
	
	[SerializeField] private RootDisplayController rootDisplaycontroller;
	[SerializeField] private HexUIController hexUIController;
	
	[SerializeField] private bool isWinningNode;
	[SerializeField] private MeshRenderer centerHex;
	[SerializeField] private MeshRenderer botLeft;
	[SerializeField] private MeshRenderer botRight;

	[SerializeField] private Material defaultMaterial;
	[SerializeField] private Material defaultHexMaterial;
	[SerializeField] private Material endGameMaterial;
	[SerializeField] private Material lockedMaterial;
	[SerializeField] private Material onHoverMaterial;
	[SerializeField] private Material captureIndicationMaterial;
	[SerializeField] private Material bedrockNodeMaterial;
	[SerializeField] private Material blockedNodeMaterial;
	[SerializeField] private bool isCapturable;
	[SerializeField] private bool isCaptured;
	[SerializeField] private GameObject fogObject;
	[SerializeField] private int xIndex;
	[SerializeField] private int yIndex;
	[SerializeField] private CameraController cameraController;
	[SerializeField] private AudioController audioController;
	[SerializeField] private VFXController vfxController;
	
	[SerializeField] [HideInInspector] private HexMap.HexMap hexMap;

	private CoreLoop _coreLoop;

	private void ApplyNodeTypeToControllers() {
		nodeTypeHolder.NodeTypeEnum = nodeTypeEnum;
		hexUIController.NodeType = nodeTypeEnum;
		hexUIController.UpdateUIDisplay();
	}
	
	private void Start(){
		SetProperMaterial();
		nodeTypeHolder.PlayerStats = FindObjectOfType<PlayerStats>();
		nodeTypeHolder.ResolveNodeType();
		hexUIController.UpdateUIDisplay();
		cameraController = FindObjectOfType<CameraController>();
		audioController = FindObjectOfType<AudioController>();
		vfxController = FindObjectOfType<VFXController>();
		fogObject.SetActive(true);
		_coreLoop = FindObjectOfType<CoreLoop>();
	}

	private void OnMouseEnter(){
		if(!isCaptured){
			return;
		}
		
		SetMaterial(onHoverMaterial);
	}

	private void OnMouseExit(){
		SetProperMaterial();
	}

	private void OnMouseDown(){
		if(!isCapturable || isCaptured){
			return;
		}

		LockToPlayer();
		nodeTypeHolder.WasClicked();
	}

	public void LockToPlayer(){
		audioController.PlayHexLockedSound();
		vfxController.HexLocked(transform);
		
		isCaptured = true;
		cameraController.FollowTransform(transform);

		SetMaterial(lockedMaterial, centerHex);
		if(hexMap.LastBotLeftNeighbour != null){
			hexMap.LastBotLeftNeighbour.isCapturable = false;
			hexMap.LastBotLeftNeighbour.SetProperMaterial();
		}

		if(hexMap.LastBotRightNeighbour != null){
			hexMap.LastBotRightNeighbour.isCapturable = false;
			hexMap.LastBotRightNeighbour.SetProperMaterial();
		}

		var cellLeftBot = hexMap.GetCellNeighbour(this, HexDirection.BotLeft);
		if(cellLeftBot != null){
			cellLeftBot.SetVisible();
			hexMap.GetAllPossibleCellNeighbours(cellLeftBot)
				   .ForEach(node => node.SetVisible());

			cellLeftBot.IndicateCaptureOnCell(HexDirection.BotLeft);
			hexMap.LastBotLeftNeighbour = cellLeftBot;
		}

		var cellRightBot = hexMap.GetCellNeighbour(this, HexDirection.BotRight);
		if(cellRightBot != null){
			cellRightBot.SetVisible();
			hexMap.GetAllPossibleCellNeighbours(cellRightBot)
				   .ForEach(node => node.SetVisible());

			cellRightBot.IndicateCaptureOnCell(HexDirection.BotRight);
			hexMap.LastBotRightNeighbour = cellRightBot;
		}
		
		SetRootDisplayControllerChecks();
		_coreLoop.EndTurn();

		if(isWinningNode){
			_coreLoop.GameEnd();
		}
	}

	private void SetRootDisplayControllerChecks() {
		var cellTopLeft = hexMap.GetCellNeighbour(this, HexDirection.TopLeft);
		var cellTopRight = hexMap.GetCellNeighbour(this, HexDirection.TopRight);
		if (cellTopLeft is null && cellTopRight is null) {
			rootDisplaycontroller.SetUpperConnection(RootConnectionDirection.None);
			rootDisplaycontroller.EnableMainRoot();
		}

		else if (cellTopLeft.isCaptured) {
			rootDisplaycontroller.SetUpperConnection(RootConnectionDirection.Left);
			cellTopLeft.rootDisplaycontroller.SetLowerConnection(RootConnectionDirection.Right);
		}
			
		else if (cellTopRight.isCaptured) {
			rootDisplaycontroller.SetUpperConnection(RootConnectionDirection.Right);
			cellTopRight.rootDisplaycontroller.SetLowerConnection(RootConnectionDirection.Left);
		}
	}

	private void IndicateCaptureOnCell(HexDirection fromDirection){
		if(nodeTypeEnum.Equals(NodeTypeEnum.Enemy)){
			isCapturable = false;
			SetMaterial(blockedNodeMaterial, centerHex);
			return;
		}

		isCapturable = true;
		var materialToSet = captureIndicationMaterial;
		if(isWinningNode){
			materialToSet = endGameMaterial;
		}

		switch(fromDirection){
			case HexDirection.BotLeft:
				SetMaterial(materialToSet, centerHex);
				break;
			case HexDirection.BotRight:
				SetMaterial(materialToSet, centerHex);
				break;
		}
	}

	public void SetVisible(){
		fogObject.SetActive(false);
	}

	private void SetProperMaterial(){
		if(isWinningNode){
			SetMaterial(endGameMaterial, centerHex);
		} else if(nodeTypeEnum.Equals(NodeTypeEnum.Enemy)){
			SetMaterial(bedrockNodeMaterial, centerHex);
		} else if(isCaptured){
			SetMaterial(lockedMaterial);
		} else if(isCapturable){
			SetMaterial(captureIndicationMaterial, centerHex);
		} else{
			SetMaterial(defaultMaterial);
			SetMaterial(defaultHexMaterial, centerHex);
		}
	}

	private void OnValidate() {
		ApplyNodeTypeToControllers();
	}

	private void SetMaterial(Material material){
		botLeft.material = material;
		botRight.material = material;
	}

	private void SetMaterial(Material material, MeshRenderer meshRenderer){
		meshRenderer.material = material;
	}

	public HexMap.HexMap HexMap{
		get => hexMap;
		set => hexMap = value;
	}

	public int XIndex{
		get => xIndex;
		set => xIndex = value;
	}
	public int YIndex{
		get => yIndex;
		set => yIndex = value;
	}

	public bool IsWinningNode{
		get => isWinningNode;
		set => isWinningNode = value;
	}
}
